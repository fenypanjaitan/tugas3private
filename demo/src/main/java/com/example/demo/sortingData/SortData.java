package com.example.demo.sortingData;


import java.util.Arrays;
import java.util.Collections;

import static java.util.Arrays.sort;
import static java.util.Collections.reverseOrder;

public class SortData {
    public static void main(String[] args) {
        Integer[] acak = {1,4,6,2,5,8,3,7,9};

        System.out.println("Data Asli: ");
        for (int i = 0; i <acak.length ; i++) {
            System.out.print(acak[i]+"\t");
        }

        System.out.println();
        System.out.println("Pengurutan Ascending: ");
        sort(acak);
        for (int i = 0; i <acak.length ; i++) {
            System.out.print(acak[i]+ "\t");
        }

        System.out.println();
        System.out.println("Pengurutan Descending: ");
        Arrays.sort(acak, Collections.reverseOrder());
        for (int i = 0; i <acak.length ; i++) {
            System.out.print(acak[i]+ "\t");
        }

    }
}
