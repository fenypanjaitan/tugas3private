package com.example.demo.diskonBelanjaan;

import java.util.Scanner;

public class Diskon {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Masukkan harga Pembelian: ");
        double hargaPembelian = scanner.nextDouble();
        System.out.println("========== HASIL ===========");
        System.out.println("pembelian: "+hargaPembelian);
        double diskon = hargaPembelian * 10/100; //10%
        System.out.println("diskon: "+ diskon);
        double hargaBayar = hargaPembelian - diskon;
        System.out.println("harga Bayar: "+hargaBayar);

    }
}
