package com.example.demo.menuRestaurant;

import java.util.Scanner;

public class Menu {
    public static void main(String[] args) {
        System.out.println("================ MENU RESTAURANT ================ ");
        System.out.println("1. Nasi Goreng Biasa \t\t\t Rp. 13.000,_");
        System.out.println("2. Nasi Ayam Bakar \t\t\t\t Rp. 15.000,_");
        System.out.println("3. Nasi Ayam Goreng \t\t\t Rp. 15.000,_");
        System.out.println("4. Nasi Goreng Gila \t\t\t Rp. 20.000,_");
        System.out.println("================================================= ");

        Scanner scanner =new Scanner(System.in);
        System.out.print("Masukkan pilihan anda: ");
        int pilihan = scanner.nextInt();

        switch (pilihan) {
            case 1:
                System.out.println("Pilihan No.1 Nasi Goreng Biasa Rp. 13.000,_");
                break;
            case 2:
                System.out.println("Pilihan No.2 Nasi Ayam Bakar Rp. 15.000,_");
                break;
            case 3:
                System.out.println("Pilihan No.3 Nasi Ayam Goreng Rp. 15.000,_");
                break;
            case 4:
                System.out.println("Pilihan No.4 Nasi Goreng Gila Rp. 20.000,_");
                break;
            default:
                System.out.println("Pilihan tidak tersedia");
        }




    }
}
