package com.example.demo.musim;

import java.util.Scanner;

public class Musim {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Masukkan bulan: ");
        String bulan = scanner.next();

        if(bulan.equalsIgnoreCase("Desember")||bulan.equalsIgnoreCase("Januari")||bulan.equalsIgnoreCase("Februari")){
            System.out.println("bulan "+bulan+" adalah musim dingin");
        } else if(bulan.equalsIgnoreCase("Maret")||bulan.equalsIgnoreCase("April")||bulan.equalsIgnoreCase("Mei")){
            System.out.println("bulan "+bulan+" adalah musim semi");
        } else if(bulan.equalsIgnoreCase("Juni")||bulan.equalsIgnoreCase("Juli")||bulan.equalsIgnoreCase("Agustus")){
            System.out.println("bulan "+bulan+" adalah musim panas");
        } else if(bulan.equalsIgnoreCase("September")||bulan.equalsIgnoreCase("Oktober")||bulan.equalsIgnoreCase("November")){
            System.out.println("bulan "+bulan+" adalah musim gugur");
        } else {
            System.out.println("tidak ada bulan "+bulan);
        }

    }
}
