package com.example.demo.perpustakaan.buku;

import java.util.ArrayList;

public class Novel extends Buku {

    public Novel(int id, String judul, String pengarang) {
        super(id, judul, pengarang);
    }

    public void novels(){
        ArrayList<Novel> novels = new ArrayList<>();
        novels.add(new Novel(1,"Negeri Para Bedebah","TereLiye"));
        novels.add(new Novel(2,"Negeri di Ujung Tanduk","TereLiye"));
        novels.add(new Novel(3,"Filosofi Kopi","Dee Lestari"));
        novels.add(new Novel(4,"SuperNova Partikel","Dee Lestari"));
        novels.add(new Novel(5,"Pada suatu hari nanti","Sapardi Djoko Damono"));

    }



}
