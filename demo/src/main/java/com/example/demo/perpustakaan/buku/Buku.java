package com.example.demo.perpustakaan.buku;

public class Buku {
    private int id;
    private String judul;
    private String pengarang;

    public Buku(int id, String judul, String pengarang) {
        this.id = id;
        this.judul = judul;
        this.pengarang = pengarang;
    }
}
