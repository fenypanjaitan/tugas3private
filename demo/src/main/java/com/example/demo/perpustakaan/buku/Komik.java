package com.example.demo.perpustakaan.buku;

import java.util.ArrayList;

public class Komik extends Buku{
    public Komik(int id, String judul, String pengarang) {
        super(id, judul, pengarang);
    }

    public void komiks(){
        ArrayList<Komik> komiks = new ArrayList<>();
        komiks.add(new Komik(1,"Komik1","Author1"));
        komiks.add(new Komik(2,"Komik2","Author2"));
        komiks.add(new Komik(3,"Komik3","Author3"));
        komiks.add(new Komik(4,"Komik4","Author4"));
        komiks.add(new Komik(5,"Komik5","Author5"));

    }
}
