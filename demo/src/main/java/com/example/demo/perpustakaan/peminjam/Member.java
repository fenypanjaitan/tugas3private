package com.example.demo.perpustakaan.peminjam;

import java.util.ArrayList;

public class Member extends Peminjam{
    private int id;

    public Member( int id,String nama, String alamat, String noHp, String email) {
        super(nama, alamat, noHp, email);
        this.id = id;
    }

    public void listMember(){
        ArrayList<Member> members = new ArrayList<>();
        members.add(new Member(1,"alpha","lima","081290909898","alpha@ns.com"));
        members.add(new Member(2,"charlie","peru","081287876767","charlie@ns.com"));
        members.add(new Member(3,"echo","india","081576867786","echo@ns.com"));
    }
}
