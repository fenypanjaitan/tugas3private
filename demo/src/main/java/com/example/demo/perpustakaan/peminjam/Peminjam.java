package com.example.demo.perpustakaan.peminjam;

public class Peminjam {
    private String nama;
    private String alamat;
    private String noHp;
    private String email;

    public Peminjam(String nama, String alamat, String noHp, String email) {
        this.nama = nama;
        this.alamat = alamat;
        this.noHp = noHp;
        this.email = email;
    }
}
