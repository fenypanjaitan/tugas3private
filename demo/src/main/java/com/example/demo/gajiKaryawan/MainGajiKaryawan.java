package com.example.demo.gajiKaryawan;

import java.util.ArrayList;
import java.util.Scanner;

public class MainGajiKaryawan {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Pilih aksi yang anda inginkan");
        System.out.println("1. Show Data \n2. Cek Tunjangan dan Total Gaji \n");
        System.out.print("aksi : ");
        int aksi = scanner.nextInt();
        System.out.println();

        ArrayList<Staff> staff = new ArrayList<>();
        staff.add(new Staff(1,"alpha",5000));
        staff.add(new Staff(2,"beta",5000));
        staff.add(new Staff(3, "charlie", 5000));

        ArrayList<Manager> managers = new ArrayList<>();
        managers.add(new Manager(4,"foxtrot",10000));
        managers.add(new Manager(5,"echo",10000));

        switch (aksi){
            case 1:
                //show data
                for (Staff value : staff) {
                    System.out.println("id : " + value.getIdKaryawan() + ", nama : " + value.getNama() + ",\t\tGaji Pokok : " + value.getGajiPokok());
                }

                for (Manager manager : managers) {
                    System.out.println("id : " + manager.getIdKaryawan() + ", nama : " + manager.getNama() + ",\t\tGaji Pokok : " + manager.getGajiPokok());
                }
                break;

            case 2:
                System.out.print("cari id: ");
                int idCari = scanner.nextInt();
                if(idCari<=staff.size()){
                    for (int i = 0; i < staff.size(); i++) {
                        int idS = idCari - 1;

                        System.out.print("masukkan absen: ");
                        staff.get(idS).setAbsen(scanner.nextInt());
                        int tjMakan = staff.get(idS).getTjMakan();
                        int gaji = staff.get(idS).getGajiPokok();
                        int tjPulsa = 100;
                        int totalGaji = gaji + tjMakan + tjPulsa;

                        System.out.println("\nid\t\t\t\t : " + staff.get(idS).getIdKaryawan());
                        System.out.println("Nama\t\t\t : " + staff.get(idS).getNama());
                        System.out.println("Jabatan\t\t\t : Staff");
                        System.out.println("Gaji Pokok\t\t : " + staff.get(idS).getGajiPokok());
                        System.out.println("Tunjangan Pulsa\t : "+tjPulsa);
                        System.out.println("Tunjangan makan\t : "+tjMakan);
                        System.out.println("Maka Total Gaji Staff Id "+idCari+" adalah: "+totalGaji);
                        break;
                    }
                } else {
                    for (int i = 0; i < managers.size(); i++) {
                        int idM = idCari - staff.size()-1;

                        System.out.print("masukkan absen: ");
                        managers.get(idM).setAbsen(scanner.nextInt());
                        System.out.print("masukkan jumlah entertain: ");
                        int jlhEntertain = scanner.nextInt();
                        int tjPulsa = 100;
                        int tjTransport = managers.get(idM).getTjTransport();
                        int tjEntertain = managers.get(idM).getTjEntertain() * jlhEntertain;
                        int gaji = managers.get(idM).getGajiPokok();
                        int totalGaji = gaji + tjTransport + tjEntertain+tjPulsa;

                        System.out.println("\nid\t\t\t\t\t : " + managers.get(idM).getIdKaryawan());
                        System.out.println("Nama\t\t\t\t : " + managers.get(idM).getNama());
                        System.out.println("Jabatan\t\t\t\t : Manager");
                        System.out.println("Gaji Pokok\t\t\t : " + managers.get(idM).getGajiPokok());
                        System.out.println("Tunjangan Pulsa\t\t : "+tjPulsa);
                        System.out.println("Tunjangan Transport\t : "+tjTransport);
                        System.out.println("Tunjangan Entertain\t : "+tjEntertain);
                        System.out.println("Maka Total Gaji Manager Id "+idCari+" adalah: "+totalGaji);
                        break;
                    }
                }
                break;
            default:
                break;
        }







    }
}
