package com.example.demo.gajiKaryawan;

public class Worker {
    private int idKaryawan;
    private String nama;
    private int tjPulsa;
    private int gajiPokok;
    private int absen;

    public Worker(int idKaryawan, String nama, int gajiPokok) {
        this.idKaryawan = idKaryawan;
        this.nama = nama;
        this.gajiPokok = gajiPokok;

    }

    public int getIdKaryawan() {
        return idKaryawan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getGajiPokok() {
        return gajiPokok;
    }

    public void setGajiPokok(int gajiPokok) {
        this.gajiPokok = gajiPokok;
    }


    public int getAbsen() {
        return absen;
    }

    public void setAbsen(int absen) {
        this.absen = absen;
    }
}
