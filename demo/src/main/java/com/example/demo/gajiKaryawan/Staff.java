package com.example.demo.gajiKaryawan;

public class Staff extends Worker {
    private int tjMakan = 20;

    public Staff(int idKaryawan, String nama,  int gajiPokok) {
        super(idKaryawan, nama, gajiPokok);
    }


    public int getTjMakan() {
        return tjMakan*getAbsen();
    }

    public void setTjMakan(int tjMakan) {
        this.tjMakan = tjMakan;
    }
}
