package com.example.demo.gajiKaryawan;

public class Manager extends Worker{
    private int tjTransport = 50;
    private int tjEntertain = 500;


    public Manager(int idKaryawan, String nama, int gajiPokok) {
        super(idKaryawan, nama, gajiPokok);
    }

    public int getTjTransport() {
        return tjTransport*getAbsen();
    }

    public void setTjTransport(int tjTransport) {
        this.tjTransport = tjTransport*getAbsen();
    }

    public int getTjEntertain() {
        return tjEntertain;
    }

    public void setTjEntertain(int tjEntertain) {
        this.tjEntertain = tjEntertain;
    }
}
