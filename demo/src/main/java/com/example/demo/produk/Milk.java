package com.example.demo.produk;

import java.util.Scanner;

public class Milk {
    int totalHarga;
    String[] nama = { "Bebelac", "Morinaga", "Dancow" };
    int[][] harga ={{200000, 150000, 100000}, //bebelac
            {150000, 130000, 70000}, //Morinaga
            {100000, 70000, 45000}}; //Dancow
    int[][] stok = {{12,10,20},
            {20, 20, 40},
            {30,30,10}};
    String[] ukuran = {"B","S","K"};


    public void belanja(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Masukkan Kode Susu (1-3): ");
        int kode = scanner.nextInt();
        System.out.print("Masukkan ukuran (B / S / K ) : ");
        String ukuran1 =scanner.next();
        System.out.print("Masukkan Jumlah Pembelian: ");
        int jumlahBeli = scanner.nextInt();
        System.out.println();

        String namaProduk = nama[kode-1];
        System.out.println("Nama Produk \t: "+namaProduk);

        int i = 0;
        for (; i <ukuran.length ; i++) {
            if(ukuran[i].equalsIgnoreCase(ukuran1)){
                break;
            }
        }

        int hargaSatuan = harga[kode-1][i];
        System.out.println("Harga Satuan \t: "+hargaSatuan);
        int stokBarang = stok[kode-1][i];

        if(jumlahBeli>stokBarang){
            System.out.println("Stok tidak cukup");
        } else {
            totalHarga = hargaSatuan*jumlahBeli;
            System.out.println("Total Bayar \t: " +totalHarga);
        }


//        System.out.println(nama[kode-1]);
//        int i = 0;
//        for (; i <ukuran.length ; i++) {
//            if(ukuran[i].equalsIgnoreCase(ukuran1)){
//                break;
//            }
//        }
//        System.out.println(harga[kode-1][i]);
//        System.out.println(stok[kode-1][i]);
//
//


//        switch (kode){
//            case 1:
//                System.out.println("Nama Produk \t: "+nama[0]);
//                break;
//            case 2:
//                System.out.println("Nama Produk \t: "+nama[1]);
//                break;
//            case 3:
//                System.out.println("Nama Produk \t: "+nama[2]);
//                break;
//            default:
//                System.out.println("tidak tersedia");
//
//        }
//
//
//
//        if(kode==1&&ukuran.equalsIgnoreCase("B")&&(jumlahBeli<stok[0][0])){
//            totalHarga = jumlahBeli*harga[0][0];
//            System.out.println("Harga Satuan \t: "+harga[0][0]);
//            System.out.println("Total Bayar \t: " +totalHarga);
//        } else if(kode==1&&ukuran.equalsIgnoreCase("S")&&(jumlahBeli<stok[0][1])){
//            totalHarga = jumlahBeli*harga[0][1];
//            System.out.println("Harga Satuan \t: "+harga[0][1]);
//            System.out.println("Total Bayar \t: " +totalHarga);
//        } else if(kode==1&&ukuran.equalsIgnoreCase("K")&&(jumlahBeli<stok[0][2])){
//            totalHarga = jumlahBeli*harga[0][2];
//            System.out.println("Harga Satuan \t: "+harga[0][2]);
//            System.out.println("Total Bayar \t: " +totalHarga);
//        } else {
//            System.out.println("Tidak jalan");
//        }


    }

}
