package com.example.demo.nilaiSiswa;

import java.util.ArrayList;
import java.util.Scanner;

public class SiswaMain {
    public static void main(String[] args) {

        ArrayList<Integer> id = new ArrayList<>();
        ArrayList<String> nama = new ArrayList<>();
        ArrayList<Integer> nilai = new ArrayList<>();
        ArrayList<String> peringkatHuruf = new ArrayList<>();
//        ArrayList<Siswa> siswa = new ArrayList<>();



        while (true) {
            System.out.println("\n=========== Menu ===========");
            System.out.println(" 1. Tambah Siswa Baru \n 2. Edit Data Siswa \n 3. Hapus Siswa \n 4. Laporan Data Siswa \n 5. Exit");
            Scanner scanner = new Scanner(System.in);
            System.out.print("Pilih aksi yang mau dilakukan: ");
            int pilihan = scanner.nextInt();

            switch (pilihan) {
                case 1:
                    System.out.print("Masukkan id: ");
                    int idSiswa = scanner.nextInt();
                    System.out.print("Masukkan Nama: ");
                    String namaSiswa = scanner.next();
                    System.out.print("Masukkan nilai: ");
                    int nilaiSiswa = scanner.nextInt();
                    id.add(idSiswa);
                    nama.add(namaSiswa);
                    nilai.add(nilaiSiswa);
//                  siswa.add(new Siswa(idSiswa,namaSiswa,nilaiSiswa));
                    String grade;

                    if (nilaiSiswa<=20) {
                        grade = "E";
                        peringkatHuruf.add(grade);
                    } else if (nilaiSiswa <= 40) {
                        grade = "D";
                        peringkatHuruf.add(grade);
                    } else if (nilaiSiswa <= 60) {
                        grade = "C";
                        peringkatHuruf.add(grade);
                    } else if (nilaiSiswa <= 80) {
                        grade = "B";
                        peringkatHuruf.add(grade);
                    } else if (nilaiSiswa <= 100) {
                        grade = "A";
                        peringkatHuruf.add(grade);
                    } else {
                        System.out.println("Tidak ada nilai sebesar "+nilaiSiswa);
                    }
                    break;
                case 2:
                    System.out.print("Cari ID : ");
                    int cari = scanner.nextInt();
                    System.out.println("Id siswa : " + id.get(cari - 1));
                    System.out.println("Nama : " + nama.get(cari - 1));
                    System.out.println("Nilai Sebelumnya : " + nilai.get(cari - 1));
                    System.out.print("Masukkan Nilai Baru : ");
                    int nilaiBaru = scanner.nextInt();
                    nilai.set(cari - 1, nilaiBaru);
                    System.out.print("Nilai Setelah Diubah : " + nilai.get(cari-1));

                    if (nilaiBaru<=20) {
                        grade = "E";
                        peringkatHuruf.set(cari-1,grade);
                    } else if (nilaiBaru <= 40) {
                        grade = "D";
                        peringkatHuruf.set(cari-1,grade);
                    } else if (nilaiBaru <= 60) {
                        grade = "C";
                        peringkatHuruf.set(cari-1,grade);
                    } else if (nilaiBaru <= 80) {
                        grade = "B";
                        peringkatHuruf.set(cari-1,grade);
                    } else if (nilaiBaru <= 100) {
                        grade = "A";
                        peringkatHuruf.set(cari-1,grade);
                    } else {
                        System.out.println("Tidak ada nilai sebesar "+nilaiBaru);
                    }
                    break;
                case 3:
                    System.out.print("Hapus ID : ");
                    int hapus = scanner.nextInt();
//                    siswa.remove(siswa.get[hapus-1]);
                    id.remove(hapus-1);
                    nama.remove(hapus-1);
                    nilai.remove(hapus-1);
                    peringkatHuruf.remove(hapus-1);
                    System.out.println("Data dihapus");
                    break;
                case 4:
                    int data = id.size();
                    if(data==0){
                        System.out.println("Data Belum Ada");
                    } else {
                        System.out.println("==================================================");
                        System.out.println("ID\t\tNAMA\t\t\tNILAI\t\tGRADE   ");
                        System.out.println("==================================================");

                        for (int i = 0; i < data; i++) {
                            System.out.print(id.get(i) + "\t\t" + nama.get(i) + "\t\t\t " + nilai.get(i) + "\t\t\t " + peringkatHuruf.get(i) + "\n");
                        }
                        System.out.println("==================================================");
                    }
                    break;
                case 5:
                    System.exit(0);
                    break;
                default:
                    System.out.println("tidak ada pilihan");
            }

        }
    }
}
