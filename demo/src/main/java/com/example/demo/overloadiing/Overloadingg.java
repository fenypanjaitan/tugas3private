package com.example.demo.overloadiing;

public class Overloadingg {
    public static void main(String[] args) {
        int a = 11;
        int b = 6;
        double c = 7.3;
        double d = 9.4;
        int result1 = minFunction(a, b);

        // same function name with different parameters
        double result2 = minFunction(c, d);
        System.out.println("Minimum Value = " + result1);
        System.out.println("Minimum Value = " + result2);
    }


    public static int minFunction(int a, int b) {
        int min;
        if (a>b)
            min = b;
        else
            min = a;

        return min;
    }

    public static double minFunction(double a, double b) {
        double min;
        if (a > b)
            min = b;
        else
            min = a;

        return min;
    }
}
