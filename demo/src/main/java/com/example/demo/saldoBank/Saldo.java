package com.example.demo.saldoBank;

import java.util.Scanner;

public class Saldo {
    public static void main(String[] args) {
        double bunga = 0.02;  //2%
        double biayaAdmin = 15000;

        Scanner scanner = new Scanner(System.in);
        System.out.print("Masukkan Saldo anda: ");
        double saldo = scanner.nextDouble();
        System.out.println("Saldo awal: "+saldo);

        double totalSaldo = saldo + (saldo*bunga) - biayaAdmin;
        System.out.println("Jumlah saldo setelah satu bulan adalah: "+totalSaldo);

    }
}
