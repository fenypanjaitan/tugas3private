package com.example.demo.laporanPenjualan;

import java.io.DataInputStream;

public class Penjualan1 {
    public static void main(String[] args) throws Exception {
        String[] namaBarang = new String[50];
        int[] jumlahPenjualan = new int[100];
        int[] harga = new int[100];

        DataInputStream karakter = new DataInputStream(System.in);
        System.out.print("Masukan Bulan Penjualan : ");
        String bulan=karakter.readLine();
        System.out.print("Masukan Jumlah Data : ");
        String jml=karakter.readLine();
        int data = Integer.parseInt(jml); //Integer.valueOf(jml).intValue();

        for(int i=0;i<data;i++) {
            System.out.print("Nama Barang Ke-"+(i+1)+" = ");
            String nama=karakter.readLine();
            namaBarang[i]=nama;
            System.out.print("Jumlah Terjual: ");
            String jlh=karakter.readLine();
            int jlah = Integer.valueOf(jlh).intValue();
            jumlahPenjualan[i]=jlah;
            System.out.print("Harga Satuan: ");
            String har=karakter.readLine();
            int harg = Integer.valueOf(har).intValue();
            harga[i]=harg;
        }

        System.out.println();
        System.out.println("LAPORAN PENJUALAN PT. NexSOFT");

        System.out.println("BULAN : "+bulan);

        System.out.println("================================================");

        System.out.println("NO  NAMA BARANG     JUMLAH    HARGA    TOTAL    ");

        System.out.println("================================================");

        int totalPenjualan=0;

        for(int i=0;i<data;i++) {
            System.out.println((i+1)+"\t"+namaBarang[i]+"\t\t\t"+jumlahPenjualan[i]+"\t\t"+harga[i]+"\t\t"+(jumlahPenjualan[i]*harga[i]));
            totalPenjualan = totalPenjualan + (jumlahPenjualan[i]*harga[i]);
        }

        System.out.println("================================================");

        System.out.println("TOTAL BARANG : "+data);

        System.out.println("TOTAL PENJUALAN : "+totalPenjualan);


    }
}
