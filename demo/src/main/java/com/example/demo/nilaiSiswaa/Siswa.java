package com.example.demo.nilaiSiswaa;


import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Siswa {
    List<Siswa> siswas = new List<Siswa>() {
        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @Override
        public Iterator<Siswa> iterator() {
            return null;
        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return null;
        }

        @Override
        public boolean add(Siswa siswa) {
            return false;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends Siswa> c) {
            return false;
        }

        @Override
        public boolean addAll(int index, Collection<? extends Siswa> c) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {

        }

        @Override
        public Siswa get(int index) {
            return null;
        }

        @Override
        public Siswa set(int index, Siswa element) {
            return null;
        }

        @Override
        public void add(int index, Siswa element) {

        }

        @Override
        public Siswa remove(int index) {
            return null;
        }

        @Override
        public int indexOf(Object o) {
            return 0;
        }

        @Override
        public int lastIndexOf(Object o) {
            return 0;
        }

        @Override
        public ListIterator<Siswa> listIterator() {
            return null;
        }

        @Override
        public ListIterator<Siswa> listIterator(int index) {
            return null;
        }

        @Override
        public List<Siswa> subList(int fromIndex, int toIndex) {
            return null;
        }
    };
}
